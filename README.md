# Ⱂⱃⱆⰳⰰ (Pruga) vscode extension

> English version of this file for global English language users did created by automated translation and is available in the file [README-en.md](https://codeberg.org/rodokrug/pruga-vscode/src/branch/master/README-en.md).

Toto rozšíření dělá jednoduchou, ale velice mocnou věc. Z editoru zdrojových kódů udělá vývojové prostředí neomezených možností.


Celé je to postavené na úplně primitivní myšlence. Klávesovou skratkou spustíš skript v terminálu. Předtím uloží soubor, který právě upravuješ.

Skript jsem nazval `./make.fish`, protože používám *fish shell*, ale můžeš stejně tak použít `./make.sh` pro běžně používaný *bash shell* a nebo něco, co funguje v tvém počítači, ať už v tvém linuxu, na windows, či apple.

> V zásadě příliš nezáleží na tom, jaký shell použiješ. Ačkoliv je shell zároveň programovacím jazykem, osobně se snažím mít v tomto skriptu jen velice stručný kód, ideálně jeden řádek, který spustí jiný nástroj, kompilátor, make, webpack, ... . Skript `make.fish` je však dobrá mezivrstva, rád zkouším různé věci a občas narazím na něco, co je potřeba si v příkazovém řádku opakovaně spouštět. A v tomto skriptu si to zapíšu a spouštím a průběžně upravuji.

```sh
#!/usr/bin/env fish

# Here you can write shell script code

echo "Ⱂⱃⱆⰳⰰ is the simplest and very powerfull 'build system'"

echo 'SHELL: Ⱂⱃⱆⰳⰰ run make.fish'

# comment unused commands

# Do you use make for build your program?
#make

# Do you not like shell? Are you like python? Your make.fish can run play.py.

# python /play.py $1
# or with shebank #!/usr/bin/env python
./play.py $1

# Do you use PHP?
php ./play.php $1

# Do you like excellent Nim language?
nim compile --app:console -d:release --out:bin/pruga-make ./cli/pruga/make.nim

```

If you use Python, your make.fish can run play.py.

```python
#!/usr/bin/env python

import sys

print('PYTHON: Ⱂⱃⱆⰳⰰ spouští play.py')
print(sys.argv)

```

PHP is better for you?


```php
<?php

echo "Ⱂⱃⱆⰳⰰ spouští play.php\n";
echo join(' ', $argv);
echo "\n";

```

Nim is better for you?


```nim

import std/terminal

styledEcho fgBlue, "I like", fgYellow, "Nim"

```

You can use any commands, languages, scripts ...


## projekt a soubor

Praxe ukázala, že je dobré mít dva skripty namapované na dvě klávesové zkratky. Ty skripty jsou si podobné

- make.fish
- play.fish

záleží na tobě, co si do nich napíšeš a jak je použiješ. Je to velice konfigurovatelné, ale smysl dává toto.

- make.fish: se spustí zkratkou ctrl+shift+c a ve skriptu mám příkazy pro sestavení celého projektu. Často upravuješ soubor, který je importován hlavním souborem a potřebuješ rychlým stiskem klávesy celý projekt zkompilovat.

- play.fish: se spustí zkratkou ctrl+shift+s a ve skriptu mám příkaz pro spuštění, nebo kompilaci právě uloženého souboru.


## pruga-make

`pruga-make` is program mainly for compiling `Nim` programs with several presets:

- `nim` files from './cli' directory compile to './bin' directory 
- `nim` files from './pkg' directory compile to './tests' directory and run this script as test
- other `nim` files compile usually
- `python` and `php` files run in terminal

## Default keybinding

pro spuštění `make.fish`:

`c` - as *compile*

* mac: cmd+shift+c
* window-linux: ctrl+shift+c

pro spuštění `play.fish`:

`s` - as *save*

* mac: cmd+shift+s
* window-linux: ctrl+shift+s
