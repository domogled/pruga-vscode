mode = ScriptMode.Verbose
# Silent,                   ## Be silent.
# Verbose,                  ## Be verbose.
# Whatif                     ## Do not run commands, instead just echo what
#                             ## would have been done.

import std/os
import std/strformat

task publish, "package extension":

    exec "vsce publish"

task package, "package extension":

    exec "vsce package"

task make, "make projekt":

    exec "nim js --outdir:out --checks:on --sourceMap src/prugavscode.nim"

    packageTask()
    publishTask()
    

task play, "play current file":

    let argv = commandLineParams()

    assert argv.len() == 1, "zadej alespoň název právě upraveného souboru"
    echo argv
    echo &"pruga-make {argv[0]}"
